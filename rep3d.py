#!/usr/bin/env python
import argparse
import pathlib
import matplotlib.pyplot as plt
import numpy as np
import os

class UnorderedTuple(tuple):
	def __hash__(self):
		return hash(tuple(sorted(self)))

def compare(oldval, key, newval):
	if key == "rmsd":
		return newval < oldval
	else:
		return newval > oldval

def autoparse(value):
	try: return int(value)
	except ValueError:
		try: return float(value)
		except ValueError:
			try: return float(value[:-1])/100
			except ValueError:
				return value

def main(args):
	if args.scope.lower().startswith("st"):
		scope = "structure"
	elif args.scope.lower().startswith("ch"):
		scope = "chain"
	else:
		raise NotImplementedError

	keys = {}
	table = {}

	for fp in args.infile:
		with open(fp) as fh:
			for l in fh:
				if l.startswith("#"): 
					if not keys: keys = dict(zip(l[1:].strip().split("\t"), range(len(l[1:].strip().split("\t")))))
					continue
				elif not l.strip(): continue

				sl = l.split("\t")
				row = dict(zip(keys, sl))

				if args.disallow_identical and (not args.keep_identical) and (row["query"] == row["subject"]) and (row["qchain"] == row["schain"]) and (row["qhel"] == row["shel"]): continue

				if scope == "structure":
					key = tuple([row["query"], row["subject"]])
				elif scope == "chain":
					key = tuple(["{}_{}".format(row["query"], row["qchain"]), "{}_{}".format(row["subject"], row["schain"])])
				else: raise NotImplementedError

				value = autoparse(row[args.key])

				if key not in table: table[key] = {}

				qspan = tuple([int(x) for x in row["qhel"][1:].split("-")])
				sspan = tuple([int(x) for x in row["shel"][1:].split("-")])

				bundle = tuple([qspan, sspan])
				if bundle not in table[key]: 
					table[key][bundle] = value
				elif compare(table[key][bundle], args.key, value):
					table[key][bundle] = value
	for objpair in table:
		obj1, obj2 = objpair
		keyarr = np.array(list(table[objpair].keys()))

		max1 = np.max(keyarr[:,0,:])
		max2 = np.max(keyarr[:,1,:])

		outarr = np.zeros((max1,max2))

		outarr[:,:] = np.nan
		for (qh1, qh2), (sh1, sh2) in table[objpair]:
			outarr[qh1,sh1] = table[objpair][(qh1,qh2), (sh1, sh2)]
		fig, ax = plt.subplots()
		if args.range is not (None, None): (vmin, vmax) = args.range
		elif args.key == "quality": (vmin, vmax) = (0.0, 1.0)
		elif args.key.endswith("cov"): (vmin, vmax) = (0.0, 1.0)
		else: (vmin, vmax) = (None, None)
		fig.colorbar(ax.imshow(outarr, cmap=args.cmap, vmin=vmin, vmax=vmax))
		ax.set_xlabel(obj2)
		ax.set_ylabel(obj1)
		ax.set_title("{} for {} vs. {}".format(args.key.title(), obj1, obj2))
		
		print(obj1, obj2)
		print(max1, max2)
		if args.outdir is not None:
			if not os.path.isdir(args.outdir): os.mkdir(args.outdir)
			fig.savefig("{}/{}_vs_{}.png".format(args.outdir, obj1, obj2), dpi=300)
		if args.show: plt.show()
		plt.close()
		#input("press any key to continue")
		

	

if __name__ == "__main__":
	parser = argparse.ArgumentParser()

	parser.add_argument("infile", nargs="+", type=pathlib.Path,
		help="Deuterocol table(s) to parse")
	parser.add_argument("--scope", default="structure",
		help="Accepts one of 'class' (NYI), 'subclass' (NYI), 'family' (NYI), 'subfamily' (NYI), 'system' (NYI), 'structure' (default), or 'chain'")
	parser.add_argument("--key", default="quality",
		help="What value to plot. (default: quality)")
	parser.add_argument("--keep-identical", action="store_true",
		help="Keep results for which the query and target bundle are exactly identical")
	parser.add_argument("-o", "--outdir", 
		help="Where to save the figures")
	parser.add_argument("--show", action="store_true",
		help="Open interactive Matplotlib plots")
	parser.add_argument("--range", type=float, nargs=2, default=(None, None),
		help="Cmap range")
	parser.add_argument("--cmap", default="viridis",
		help="Cmap (default: viridis)")
	parser.add_argument("--disallow-identical", action="store_true",
		help="Blank out alignments between identical bundles from the same structure and chain")

	args = parser.parse_args()

	main(args)
