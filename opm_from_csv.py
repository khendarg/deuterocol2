#!/usr/bin/env python
import csv
import argparse
import pathlib
import re
import urllib.request
import gzip
import sys
import os
import subprocess
import json
import numpy as np
from Bio import PDB, SeqIO, Seq, Align
from Bio.Align import substitution_matrices

import tqdm

from libdeuterocol.dbtool import opm

try: from kevtools_common.types import TCID
except ImportError: 
	TCID = str
	warn("Could not import kevtools_common. TCID sorting will be unavailable.")

pdbpattern = re.compile("[0-9][0-9A-Za-z]{3}")
MIN_COVERAGE = 0.5

def download(url, path, unzip=False, force=False):
	if path.exists() and not force: return
	remote = urllib.request.urlopen(url).read()

	if unzip: content = gzip.decompress(remote)
	else: content = remote

	with open(path, "wb") as fh:
		fh.write(content)

def hamming(i1, i2): return sum((i1[_] != i2[_] for _ in range(min(len(i1), len(i2)))))

def info(*stuff):
	print("[INFO]", *stuff, file=sys.stderr)
def warn(*stuff):
	print("[WARNING]", *stuff, file=sys.stderr)


class sliceabledict(dict):
	def __getitem__(self, index):
		if type(index) is slice:
			slargs = []
			if index.start is not None: 
				slargs.append(index.start)
				if index.stop is not None: 
					slargs.append(index.stop)
					if index.step is not None: 
						slargs.append(index.step)
			return [self.get(i) for i in range(*slargs) if i in self]
		else: super().__getitem__(index)

class CSVOPMTMData(opm.OPMTMData):
	assignments = None
	verbosity = 0

	def __init__(self, *args, **kwargs):
		super().__init__(*args, **kwargs)

		self.dbpath.mkdir(exist_ok=True)
		self.pdb2fp = {}

	def parse(self, subunits):
		self.parse_subunits(subunits)

	def parse_subunits(self, subunits, pdb):
		assignments = {}
		self.dbpath.joinpath("structures").mkdir(exist_ok=True)
		self.dbpath.joinpath("structures/pdb").mkdir(exist_ok=True)
		for fn in pdb.glob("[0-9][0-9a-z][0-9a-z][0-9a-z].pdb"):
			if not self.dbpath.joinpath("structures/pdb/{}".format(fn.name)).exists():
				os.symlink(fn.absolute(), self.dbpath.joinpath("structures/pdb/{}".format(fn.name)), target_is_directory=True)
		dlme = set()
		with open(subunits) as fh:
			csv_reader = csv.reader(fh)
			fields = []
			for row in csv_reader: 
				if not fields:
					header = False
					fields = row
				else:
					rowdict = dict(zip(fields, row))
					pdbid = re.findall(pdbpattern, rowdict["pdbid"])[0]
					chain = rowdict["protein_letter"]
					pdbc = "{}_{}".format(pdbid, chain)

					segment = re.sub("[^-() 0-9,A-Z]", "", rowdict["segment"])

					rawindices = [int(x) for x in re.findall("[0-9]+", segment)]

					indices = [x for i, x in enumerate(rawindices) if i % 3]
					assert (len(rawindices) % 3) == 0, "Malformed segment: '{}'".format(rowdict["segment"])
					assert (len(indices) % 2) == 0, "Malformed segment: '{}'".format(rowdict["segment"])
					#raw:
					#assignments[pdbc] = [indices[i:i+2] for i in range(0, len(indices), 2)]
					#range arguments:
					assignments[pdbc] = [[indices[i], indices[i+1]+1] for i in range(0, len(indices), 2)]

					if pdbid not in self.pdb2fp:
						fp = pdb.joinpath("{}.pdb".format(pdbid))
						if fp.exists(): self.pdb2fp[pdbid] = fp
						else: 
							fp = self.dbpath.joinpath("structures/pdb/{}.pdb".format(pdbid))
							if fp.exists(): self.pdb2fp[pdbid] = fp
							else: dlme.add(pdbid)

		if self.verbosity > 0: info("Downloading {} missing PDBs".format(len(dlme)))
		for pdbid in sorted(dlme):
			try: 
				fp = self.dbpath.joinpath("structures/pdb/{}.pdb".format(pdbid))
				download("https://files.rcsb.org/download/{}.pdb.gz".format(pdbid), fp, unzip=True)
				if pdbid == "6wot": print("#" * 80, pdbid, fp)
				self.pdb2fp[pdbid] = fp
			except urllib.error.HTTPError as e:
				pass
			except ZeroDivisionError: 
				if "404" in str(e):
					try:
						self.dbpath.joinpath("structures/cif").mkdir(exist_ok=True)
						fpcif = self.dbpath.joinpath("structures/cif/{}.cif".format(pdbid))
						#XXX force kwarg?
						download("https://files.rcsb.org/download/{}.cif.gz".format(pdbid.upper()), fpcif, unzip=True)
						parser = PDB.MMCIFParser()
						structure = parser.get_structure(pdbid, fpcif)
						io = PDB.PDBIO()
						io.set_structure(structure)
						with open(fp, "w") as fh: io.save(fh) #as PDB
					except ZeroDivisionError: pass
		self.assignments = assignments
		self.dbpath.joinpath("assignments").mkdir(exist_ok=True)
	
		entries = []
		for pdbc in sorted(self.assignments):
			entries.append('"{pdbc}": {segments}'.format(pdbc=pdbc, segments=self.assignments[pdbc]))
		out = "{\n"
		out += ",\n".join(entries)
		out += "\n}"
		with gzip.open(self.dbpath.joinpath("assignments/opm_tms.json.gz"), "wt") as fh:
			fh.write(out)

		#legacy format
		with open(self.dbpath.joinpath("assignments/opm_tms.tsv"), "w") as fh:
			for pdbc in sorted(self.assignments):
				fh.write("{pdbc}\t{segments}\n".format(pdbc=pdbc, segments=",".join(["{}-{}".format(*span) for span in assignments[pdbc]])))

	def link_related(self, subunits=None, related=None, proteins=None):
		if not self.assignments: self.parse_subunits(subunits)

		with open(subunits) as fh:
			csv_reader = csv.reader(fh)

			fields = []
			self.pdb2pdbc = {}
			for row in csv_reader:
				if not fields:
					header = False
					fields = row
				else:
					rowdict = dict(zip(fields, row))

					pdbid = re.findall(pdbpattern, rowdict["pdbid"])[0]
					chain = rowdict["protein_letter"]
					pdbc = "{}_{}".format(pdbid, chain)
					if pdbid not in self.pdb2pdbc: self.pdb2pdbc[pdbid] = []
					self.pdb2pdbc[pdbid].append(pdbc)

		with open(proteins) as fh:
			csv_reader = csv.reader(fh)
			fields = []
			opmid2pdb = {}
			self.nontm = set()
			for row in csv_reader:
				if not fields:
					header = False
					fields = row
				else:
					rowdict = dict(zip(fields, row))
					pdbid = re.findall(pdbpattern, rowdict["pdbid"])[0]

					if int(rowdict["subunit_segments"]) == 0: self.nontm.add(pdbid)
					opmid2pdb[rowdict["id"]] = pdbid

		with open(related) as fh:
			csv_reader = csv.reader(fh)

			fields = []
			self.related = {}
			for row in csv_reader:
				if not fields:
					header = False
					fields = row

				else:
					rowdict = dict(zip(fields, row))
					if opmid2pdb[rowdict["primary_structure_id"]] in self.nontm: continue
					primary = opmid2pdb[rowdict["primary_structure_id"]]
					if primary not in self.related:
						self.related[primary] = []
					self.related[primary].extend(re.findall(pdbpattern, rowdict["pdbid"]))

		out = "{\n"
		rows = ['"{primary}": {secondaries}'.format(primary=primary, secondaries=str(sorted(self.related[primary])).replace("'", '"')) for primary in sorted(self.related)]
		out += ",\n".join(rows)
		out += "\n}"
		with gzip.open(self.dbpath.joinpath("assignments/opm_related.json.gz"), "wt") as fh:
			fh.write(out)

	def fetch_related(self, pdb=None):
		""" Fetch PDBs not processed by OPM """

		self.dbpath.joinpath("structures/related_pdb").mkdir(exist_ok=True)
		dlme = set()
		for primary in self.related:
			assert pdb.joinpath("{}.pdb".format(primary)).exists(), "Missing primary PDB. Is the pdb directory complete and recent?"
			self.pdb2fp[primary] = pdb.joinpath("{}.pdb".format(primary))
			for secondary in self.related[primary]:
				#print(secondary, pdb.joinpath("{}.pdb".format(secondary)).exists())
				if pdb.joinpath("{}.pdb".format(secondary)).exists(): self.pdb2fp[secondary] = pdb.joinpath("{}.pdb".format(secondary))
				elif not self.dbpath.joinpath("structures/related_pdb/{}.pdb".format(secondary)).exists(): dlme.add(secondary)

		dlme = sorted(dlme)
		if self.verbosity > 0: info("Downloading {} undownloaded related PDBs".format(len(dlme)))
		self.obsolete = []
		if self.dbpath.joinpath("structures/related_pdb/obsolete.pdblist").exists():
			with open(self.dbpath.joinpath("structures/related_pdb/obsolete.pdblist")) as fh:
				self.obsolete = [l.strip() for l in fh]
		for secondary in dlme:
			if secondary in self.obsolete: continue
			try: 
				print("https://files.rcsb.org/download/{}.pdb.gz".format(secondary), self.dbpath.joinpath("structures/related_pdb/{}.pdb".format(secondary)))
				download("https://files.rcsb.org/download/{}.pdb.gz".format(secondary), self.dbpath.joinpath("structures/related_pdb/{}.pdb".format(secondary)), unzip=True)
				self.pdb2fp[secondary] = self.dbpath.joinpath("structures/related_pdb/{}.pdb".format(secondary))
			except urllib.error.HTTPError as e:
				if "404" in str(e):
					self.obsolete.append(secondary)
				else: raise e
		with open(self.dbpath.joinpath("structures/related_pdb/obsolete.pdblist"), "w") as fh:
			[fh.write("{}\n".format(secondary)) for secondary in self.obsolete]

		for secondary in self.obsolete: 
			try: self.pdb2fp.pop(secondary)
			except KeyError: pass

		for primary in self.related:
			for secondary in self.related[primary]:
				if pdb.joinpath("{}.pdb".format(secondary)).exists(): self.pdb2fp[secondary] = pdb.joinpath("{}.pdb".format(secondary))
				elif self.dbpath.joinpath("structures/related_pdb/{}.pdb".format(secondary)).exists(): self.pdb2fp[secondary] = self.dbpath.joinpath("structures/related_pdb/{}.pdb".format(secondary))
				elif pdb in self.obsolete: pass

				#else: raise FileNotFoundError("No structure found for {}".format(secondary))

	def extract_sequences(self, pdb=None, force=False):
		self.dbpath.joinpath("sequences").mkdir(exist_ok=True)
		if not force:
			if self.dbpath.joinpath("sequences/inserty.faa").exists() and self.dbpath.joinpath("sequences/gappy.faa").exists() and self.dbpath.joinpath("sequences/gapless.faa").exists(): return
		#fgappy = open(self.dbpath.joinpath("sequences/gappy.faa"), "w")
		#finserty = open(self.dbpath.joinpath("sequences/inserty.faa"), "w")

		#variant formats in descending order of perversity
		inserty = []
		gappy = []
		gapless = []

		outindicesrows = []
		for pdbid in sorted(self.pdb2fp):
			i, g, gl, indices = self._parse_atom_seq(self.pdb2fp[pdbid])
			for chain in i:
				pdbc = "{}_{}".format(pdbid, chain)
				outindicesrows.append('"{pdbc}": {indices}'.format(pdbc=pdbc, indices=indices[chain]))

				inserty.append(SeqIO.SeqRecord(seq=Seq.Seq(i[chain]), id=pdbc, description=""))
				gappy.append(SeqIO.SeqRecord(seq=Seq.Seq(g[chain]), id=pdbc, description=""))
				gapless.append(SeqIO.SeqRecord(seq=Seq.Seq(gl[chain]), id=pdbc, description=""))

		outindices = "{"
		outindices += ",\n".join(outindicesrows)
		outindices += "}"

		SeqIO.write(inserty, self.dbpath.joinpath("sequences/inserty.faa"), "fasta")
		SeqIO.write(gappy, self.dbpath.joinpath("sequences/gappy.faa"), "fasta")
		SeqIO.write(gapless, self.dbpath.joinpath("sequences/gapless.faa"), "fasta")
		with gzip.open(self.dbpath.joinpath("assignments/opm_resolved.json.gz"), "wt") as fh:
			fh.write(outindices)

	def _get_backbone_coords(self, fp):
		coords = {}
		with open(fp) as fh:
			for l in fh:
				if l.startswith("ATOM") or l.startswith("HETATM"):
					letter = PDB.protein_letters_3to1.get(l[17:20])
					if letter is None: continue

					resi = int(l[22:26])
					chain = l[21]
					name = l[13:15].strip()
					if name != "CA": continue #FIXME: use N and C too
					if chain not in coords: coords[chain] = sliceabledict()
					coords[chain][resi] = [float(l[30:38]), float(l[38:46]), float(l[46:54])]

		return coords

	def _get_stride(self, fp):
		errlog = open(self.dbpath.joinpath("assignments/stride_errors.log"), "w")
		try: 
			p = subprocess.Popen(["stride", str(fp)], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
			out, err = p.communicate()
			out = out.decode("ascii")
			if err.strip(): errlog.write(err.decode("ascii"))
			
		except FileNotFoundError:
			print("Could not find stride", file=sys.stderr)
			exit(1)
		except subprocess.CalledProcessError:
			try:
				with open(fp) as fh:
					coords = ""
					for l in fh:
						if l.startswith("ATOM") or l.startswith("HETATM"): coords += l
					p = subprocess.Popen(["stride", "/dev/stdin"], stdin=subprocess.PIPE, stdout=subprocess.PIPE)

					out, err = p.communicate(input=coords.encode("ascii"))
					out = out.decode("ascii")
					if err.strip(): errlog.write(err.decode("ascii"))
			except ZeroDivisionError: pass #TODO: replace with subprocess.CalledProcessError after figuring out what's wrong with everything

		sselist = []
		for l in out.split("\n"):
			eviloffset = 0
			if l.startswith("LOC"):
				if l[59:62].strip(): continue #ignore embedded SS predictions because they can reference unresolved residues
				if l[5:15].startswith("GammaCla"):
					eviloffset = 1
				startresi = l[22+eviloffset:27+eviloffset].strip()
				endresi = l[40+eviloffset:45+eviloffset].strip()
				try:
					try: startresi = int(startresi)
					except ValueError: startresi = int(startresi[:-1])
					try: endresi = int(endresi)
					except ValueError: endresi = int(endresi[:-1])
				except ValueError as e:
					print(fp)
					print(l)
					raise e
				sselist.append({
					"type":l[5:15+eviloffset*2].strip().replace("'", "`"),
					"startresi":startresi,
					"endresi":endresi,
					"chain":l[28+eviloffset] if l[28+eviloffset] == l[46+eviloffset] else l[28+eviloffset] + l[46+eviloffset],
				})
		return sselist

	def _parse_atom_seq(self, fp):
		inserty = {}
		gappy = {}
		gapless = {}

		#dump ATOM records
		resi2resn = {}
		with open(fp) as fh:

			for l in fh:
				if l.startswith("ATOM") or l.startswith("HETATM"):
					letter = PDB.protein_letters_3to1.get(l[17:20])
					if letter is None: continue

					resi = int(l[22:26])
					chain = l[21]
					if chain not in resi2resn: resi2resn[chain] = {}
					resi2resn[chain][resi] = letter

		#process insertions/deletions/unsolved residues
		for chain in resi2resn:
			lastresi = None

			inserty[chain] = {}
			gappy[chain] = {}
			gapless[chain] = {}

			for resi in range(1, min(resi2resn[chain])):
				inserty[chain][resi] = "-"
				gappy[chain][resi] = "-"

			for resi in sorted(resi2resn[chain]):
				if lastresi is not None:
					for i in range(lastresi + 1, resi): 
						inserty[chain][i] = "-"
						gappy[chain][i] = "-"
				inserty[chain][resi] = resi2resn[chain][resi]
				gappy[chain][resi] = resi2resn[chain][resi]
				gapless[chain][resi] = resi2resn[chain][resi]

				if resi <= 0: inserty[chain][resi] = inserty[chain][resi].lower()

				lastresi = resi

		#flatten to strings
		flati = {}
		flatg = {}
		flatgl = {}
		indices = {}
		for chain in resi2resn:
			flati[chain] = "".join([inserty[chain][resi] for resi in sorted(inserty[chain])])
			flatg[chain] = "".join([gappy[chain][resi] for resi in sorted(gappy[chain])])
			flatgl[chain] = "".join([gapless[chain][resi] for resi in sorted(gapless[chain])])
			indices[chain] = self._rle(sorted(gapless[chain]))

		return flati, flatg, flatgl, indices

	def _rle(self, indices):
		""" Compress a list of indices into a list of range(?, ?, 1) arguments """
		out = []
		for i in sorted(indices):
			if not out: out.append([i, i + 1])
			else:
				if i == out[-1][-1]: out[-1][-1] += 1
				else: out.append([i, i + 1])
		return out
		
	def tcmap(self, expect=1e-5, force=False, mincov=0.9, ident=0.9):
		#if self.dbpath.joinpath("tcmap.tsv").exists() and not force: return

		#TODO: clone extractFamily functionality
		if force or not self.dbpath.joinpath("sequences/tcdb.faa").exists():
			subprocess.call(["extractFamily.pl", "-i", "all", "-o", str(self.dbpath.joinpath("sequences"))])
		else: 
			if self.verbosity > 0: info("{} already exists, skipping extractFamily".format(self.dbpath.joinpath("sequences/tcdb.faa")))

		if force or not self.dbpath.joinpath("tcblast.blast").exists():
			out = subprocess.check_output(["blastp", "-query", str(self.dbpath.joinpath("sequences/gapless.faa")), "-subject", str(self.dbpath.joinpath("sequences/tcdb.faa")), "-outfmt", "6", "-max_target_seqs", "10", "-out", str(self.dbpath.joinpath("tcblast.blast"))])
		else:
			if self.verbosity > 0: info("{} already exists, skipping OPM vs. TCDB BLAST".format(self.dbpath.joinpath("tcblast.blast")))
			return


		seqlengths = {"query":{}, "subject":{}}
		for record in SeqIO.parse(self.dbpath.joinpath("sequences/gapless.faa"), "fasta"):
			seqlengths["query"][record.id] = len(record.seq)
		for record in SeqIO.parse(self.dbpath.joinpath("sequences/tcdb.faa"), "fasta"):
			seqlengths["subject"][record.id] = len(record.seq)

		#if not (force or not (self.dbpath.joinpath("tcmap.json.gz").exists() and self.dbpath.joinpath("tcmap.tsv").exists())): return

		tcmap = {}
		seen = set()
		with open(self.dbpath.joinpath("tcblast.blast")) as fh:
			fields = "qaccver saccver pident length mismatch gapopen qstart qend sstart send evalue bitscore".split()
			types = (str,     str,    float, int,   int,     int,    int,   int, int,   int, float, float)
					
			csv_reader = csv.reader(fh, delimiter="\t")
			for row in csv_reader:
				rowdict = dict(zip(fields, [t(f) for t, f in zip(types, row)]))

				rowdict["qcov"] = round((rowdict["qend"] - rowdict["qstart"] + 1) / seqlengths["query"][rowdict["qaccver"]], 3)
				rowdict["scov"] = round((rowdict["send"] - rowdict["sstart"] + 1) / seqlengths["subject"][rowdict["saccver"]], 3)
				if rowdict["qaccver"] not in seen:
					seen.add(rowdict["qaccver"])

					tcid = TCID(rowdict["saccver"])


					if expect is not None and rowdict["evalue"] > expect:

						if (mincov is not None and ident is not None) and (rowdict["qcov"] >= mincov or rowdict["scov"] >= mincov) and (rowdict["pident"] / 100 >= ident): pass
						elif mincov is not None and (rowdict["qcov"] >= mincov or rowdict["scov"] >= mincov): pass
						elif ident is not None and (rowdict["pident"] / 100 >= ident): pass
						else:

							low = int(np.floor(np.log10(rowdict["evalue"])))
							high = int(np.floor(np.log10(rowdict["evalue"]))) + 1

							band = "1e{low:d}_1e{high:d}".format(
								low=low,
								high=high,
							)
							tcid = TCID("0.X.0.0.1-NOCLOSEHIT_{band}".format(band=band))

					if tcid not in tcmap: tcmap[tcid] = list()
					tcmap[tcid].append(rowdict)

		tcid = TCID("0.X.0.0.0-NOGOODHIT")
		for record in SeqIO.parse(self.dbpath.joinpath("sequences/gapless.faa"), "fasta"):
			if record.id not in seen:
				if tcid not in tcmap: tcmap[tcid] = list()
				rowdict = {"qaccver":record.id,
					"saccver":str(tcid),
					"pident":0.00,
					"length":0,
					"mismatch":0,
					"gapopen":0,
					"qstart":1,
					"qend":1,
					"sstart":1,
					"send":1,
					"evalue":None,
					"bitscore":0.0,
				}
				rowdict["qcov"] = 0.0 #round((rowdict["qend"] - rowdict["qstart"] + 1) / seqlengths["query"][rowdict["qaccver"]], 3)
				rowdict["scov"] = 0.0 #round((rowdict["send"] - rowdict["sstart"] + 1) / seqlengths["subject"][rowdict["saccver"]], 3)
				tcmap[tcid].append(rowdict)
					

		sortedtcmap = {}
		out = "{"
		firsttcid = True
		for tcid in sorted(tcmap):
			if firsttcid: firsttcid = False
			else: out += ",\n"
			tcmap[tcid].sort(key=lambda rowdict: (-rowdict["pident"], rowdict["bitscore"], rowdict["qaccver"]))
			sortedtcmap[str(tcid)] = tcmap[tcid]
			out += '"{tcid}": [\n'.format(tcid=tcid)
			#out += ",\n".join(["    {}".format(str(list(hit)).replace("'", '"')) for hit in tcmap[tcid]])
			out += ",\n".join(["    {}".format(str(hit).replace("'", '"').replace("None", "null")) for hit in tcmap[tcid]])
			out += "\n]"

		out += "\n}"
		
		with gzip.open(self.dbpath.joinpath("tcmap.json.gz"), "wt") as fh:
			fh.write(out)

		#legacy tsv
		with open(self.dbpath.joinpath("tcmap.tsv"), "w") as fh:
			for tcid in sorted(tcmap):
				fh.write(str(tcid) + "\t" + ",".join(sorted(rowdict["qaccver"][:4].upper() + rowdict["qaccver"][4:] for rowdict in tcmap[tcid])) + "\n")

	def extend_assignments(self, force=False):
		""" Extend segment assignments to related PDBs """

		#TODO: find a more efficient way to queue up many one-to-one pairwise alignments
		if (self.dbpath.joinpath("assignments/opm_tms_extended.tsv").exists() and self.dbpath.joinpath("assignments/opm_tms_extended.json.gz").exists()) and not force:
			if self.verbosity > 0 : info("Skipped assignment extension because it already exists")
			return

		seqdict = {}
		chains = {}
		for record in SeqIO.parse(self.dbpath.joinpath("sequences/gapless.faa"), "fasta"):
			seqdict[record.id] = record
			if record.id[:4] not in chains: chains[record.id[:4]] = []
			chains[record.id[:4]].append(record.id[5])
		with gzip.open(self.dbpath.joinpath("assignments/opm_resolved.json.gz"), "rt") as fh:
			indices = json.load(fh)

		_rescuetypes = {
			"identical":0,
			"identical_shifted":0,
			"near-identical":0,
			"near-identical_shifted":0,
			"similar":0,
			"similar_shifted":0,
			"aligned":0,
			"too_short":0,
			"notrescued":0,
		}
		aligner = Align.PairwiseAligner()
		#aligner.open_gap_score = -0.5
		#aligner.extend_gap_score = -0.1
		aligner.open_gap_score = -5
		aligner.extend_gap_score = -1
		aligner.substitution_matrix = substitution_matrices.load("BLOSUM62")

		#print(len(self.assignments))
		best = {}
		similar = set()
		relations = {}
		for primary in tqdm.tqdm(self.related):
			for primarychain in chains[primary]:
				primarypdbc = primary + "_" + primarychain
				primaryseq = seqdict[primarypdbc]
				try: primarytms = self.assignments[primarypdbc]
				#0-tms components; skip
				except KeyError: continue
				primaryindices = indices[primarypdbc]
				primarylen = sum((len(range(start, stop)) for (start, stop) in primaryindices))
				primaryflat = []
				[primaryflat.extend(range(start, stop)) for (start, stop) in primaryindices]

				if primarypdbc not in relations: relations[primarypdbc] = {}

				for secondary in self.related[primary]:

					#presumably missing structure
					if secondary not in chains: continue

					##TODO: DEBUG STUFF
					#if primary not in ("5wau", "6met", "6iol", "3rfz") and secondary not in ("5wau", "6met", "6iol", "3rfz"): continue

					for secondarychain in chains[secondary]:
						secondarypdbc = secondary + "_" + secondarychain

						if secondarypdbc in similar: continue

						secondaryseq = seqdict[secondarypdbc]
						secondaryindices = indices[secondarypdbc]
						secondarylen = sum((len(range(start, stop)) for (start, stop) in secondaryindices))
						secondaryflat = []
						[secondaryflat.extend(range(start, stop)) for (start, stop) in secondaryindices]

						if primaryseq.seq == secondaryseq.seq and primaryindices == secondaryindices:
							_rescuetypes["identical"] += 1
							relations[primarypdbc][secondarypdbc] = {"type": "identical"}

							self.assignments[secondarypdbc] = primarytms
							similar.add(secondarypdbc)
						elif primaryseq.seq == secondaryseq.seq and primarylen == secondarylen:
							_rescuetypes["identical_shifted"] += 1
							relations[primarypdbc][secondarypdbc] = {"type": "identical_shifted"}

							similar.add(secondarypdbc)
							self.assignments[secondarypdbc] = [[secondaryflat[primaryflat.index(start)], secondaryflat[primaryflat.index(stop)]] for (start, stop) in primarytms]
						elif len(primaryseq.seq) < 15 or len(secondaryseq.seq) < 15:
							_rescuetypes["too_short"] += 1
						elif primarylen == secondarylen:
							dist = hamming(primaryseq.seq, secondaryseq.seq)
							if dist <= 0.5 * primarylen: #50% identity somehow isn't that bad
								if primaryindices == secondaryindices:
									_rescuetypes["near-identical"] += 1
									relations[primarypdbc][secondarypdbc] = {"type": "near-identical"}

									self.assignments[secondarypdbc] = primarytms
									similar.add(secondarypdbc)
								else: #TODO: drop down to aligned, but it probably doesn't matter (only 187)
									#print(primaryindices)
									#print(secondaryindices)
									_rescuetypes["near-identical_shifted"] += 1
									relations[primarypdbc][secondarypdbc] = {"type": "near-identical_shifted"}

									similar.add(secondarypdbc)
									#self.assignments[secondarypdbc] = [[secondaryflat[primaryflat.index(start)], secondaryflat[primaryflat.index(stop)]] for (start, stop) in primarytms]

						else:

							alignment = next(aligner.align(primaryseq.seq, secondaryseq.seq))
							#for alignment in aligner.align(primaryseq.seq, secondaryseq.seq):
							if secondarypdbc in best and best[secondarypdbc] > alignment.score: continue
							best[secondarypdbc] = alignment.score

							if alignment.aligned[0] == alignment.aligned[1]:
								if primaryindices == secondaryindices:
									_rescuetypes["similar"] += 1
									relations[primarypdbc][secondarypdbc] = {"type": "similar"}

									self.assignments[secondarypdbc] = primarytms
								elif primarylen == secondarylen:
									_rescuetypes["similar_shifted"] += 1
									relations[primarypdbc][secondarypdbc] = {"type": "similar_shifted"}

									#self.assignments[secondarypdbc] = primarytms
							else:
								length = sum((stop - start for (start, stop) in alignment.aligned[0]))

								#this is a VERY generous threshold: an all-similarity no-identity
								#alignment will always have a mean per-residue score > 2
								#TODO: calibrate alignment threshold
								if alignment.score < 2.0 * length:
									_rescuetypes["notrescued"] += 1
									continue
								elif length / min(len(primaryseq.seq), len(secondaryseq.seq)) < MIN_COVERAGE:
									_rescuetypes["notrescued"] += 1
									continue

								#if primarypdbc.startswith("1l0l") and secondarypdbc.startswith("1be3"):
								#	print(primarypdbc, "vs", secondarypdbc)
								#	print("length", sum((stop - start for (start, stop) in alignment.aligned[0])), alignment.aligned[0])
								#	print("coverage", length / min(len(primaryseq.seq), len(secondaryseq.seq)))
								#	print("score", alignment.score)
								#	print(alignment)
								#else: continue

								_rescuetypes["aligned"] += 1
								relations[primarypdbc][secondarypdbc] = {"type": "aligned"}
								#print(primaryseq, secondaryseq)
								#print(primaryseq.seq)
								#print(secondaryseq.seq)
								#print(bestaln)
								#print(primarytms)
								#print(primaryindices)
								#print(secondaryindices)
								#exit()
									
								#1. 1_tms (atom) -> 1_tms (gapless)
								primaryindicesflat = []
								[primaryindicesflat.extend(range(start, stop)) for (start, stop) in primaryindices]
								#print(primaryindicesflat)
								primarytmsgapless = []
								for (start, stop) in primarytms:
									try: newstart = primaryindicesflat.index(start)
									except ValueError:
										for i in range(1, len(primaryindicesflat)):
											try:
												newstart = primaryindicesflat.index(start - i)
												break
											except ValueError: pass
									try: newstop = primaryindicesflat.index(stop)
									except ValueError:
										for i in range(1, len(primaryindicesflat)):
											try:
												newstop = primaryindicesflat.index(stop + i)
												break
											except ValueError: pass
									if newstart is None or newstop is None: raise TypeError
									primarytmsgapless.append([newstart, newstop])
								#print(primarytms)
								#print(primarytmsgapless)

								#2-3. 1_all (gapless) -> 2_all (gapless)
								primaryaligned, secondaryaligned = alignment.aligned
								primaryalignedflat = []
								[primaryalignedflat.extend(range(start, stop)) for (start, stop) in primaryaligned]
								secondaryalignedflat = []
								[secondaryalignedflat.extend(range(start, stop)) for (start, stop) in secondaryaligned]
								primary2secondaryaligned = dict(zip(primaryalignedflat, secondaryalignedflat))
								#print(primary2secondaryaligned)

								secondarytmsgapless = []
								for start, stop in primarytmsgapless:
									newstart = primary2secondaryaligned.get(start)
									if newstart is None: 
										for distance in range(len(primaryindicesflat)):
											for sign in -1, 1:
												candidate = start + sign * distance
												if not (0 <= candidate < len(primaryindicesflat)): continue
												if candidate in primary2secondaryaligned:
													newstart = primary2secondaryaligned[candidate]
													break
											if newstart is not None: break
									else: TypeError("????")

									newstop = primary2secondaryaligned.get(stop)
									if newstop is None: 
										for distance in range(len(primaryindicesflat)):
											for sign in -1, 1:
												candidate = stop + sign * distance
												if not (0 <= candidate < len(primaryindicesflat)): continue
												if candidate in primary2secondaryaligned:
													newstop = primary2secondaryaligned[candidate]
													break
											if newstop is not None: break
									else: TypeError("????")
									if newstart is None or newstop is None:
										print(primarypdbc, secondarypdbc, start, stop)
										print(primarytms, primarytmsgapless)
										print(alignment)
										print(primary2secondaryaligned)
										print(primaryindices)
										print(primaryindicesflat)
										raise TypeError("??????")
									secondarytmsgapless.append([newstart, newstop])

								#print(secondarytmsgapless)
								#5. 2_tms (gapless) -> 2_tms (atom)

								secondaryindicesflat = []
								[secondaryindicesflat.extend(range(start, stop)) for (start, stop) in secondaryindices]
								secondarygapless2atom = dict(zip(range(len(secondaryindicesflat)), secondaryindicesflat))

								secondarytms = []
								for (start, stop) in secondarytmsgapless:
									newstart = None
									newstop = None
									if start in secondarygapless2atom: newstart = start
									else:
										for i in range(1, len(secondarygapless2atom)):
											if start - i in secondarygapless2atom:
												newstart = start - i
												break
									if stop in secondarygapless2atom: newstop = stop
									else:
										for i in range(1, len(secondarygapless2atom)):
											if stop + i in secondarygapless2atom:
												newstop = stop + i
												break
									if newstart is None or newstop is None:
										print(primarypdbc, secondarypdbc)
										print(primarytmsgapless)
										print(alignment)
										exit()
									secondarytms.append([newstart, newstop])
									#[secondarygapless2atom[start], secondarygapless2atom[stop]] for (start, stop) in secondarytmsgapless]
								self.assignments[secondarypdbc] = secondarytms

								if primarytms and not secondarytms:
									print(primarypdbc, secondarypdbc)
									print(primarytms)
									print(alignment)
									print(primarytmsgapless)
									exit()
								#print("1_tms (atom)", primarytms)
								#print("2_tms (atom)", secondarytms)



							#print(bestaln.score)
							#print(bestaln)
							#print(dir(bestaln))
							#print(bestaln.aligned)
							#input()
		_rescuetypes["total"] = sum([_rescuetypes[k] for k in _rescuetypes])
		print(len(self.assignments))
		print(_rescuetypes)

		entries = []
		with open(self.dbpath.joinpath("assignments/opm_tms_extended.tsv"), "wt") as fh:
			for pdbc in sorted(self.assignments):
				entries.append('"{pdbc}": {segments}'.format(pdbc=pdbc, segments=self.assignments[pdbc]))
				fh.write("{}\t{}\n".format(pdbc, ",".join(["{}-{}".format(start, stop) for (start, stop) in self.assignments[pdbc]])))
		out = "{\n"
		out += ",\n".join(entries)
		out += "\n}"
		with gzip.open(self.dbpath.joinpath("assignments/opm_extended_relations.json.gz"), "wt") as fh:
			json.dump(relations, fh, indent=1)
		with gzip.open(self.dbpath.joinpath("assignments/opm_tms_extended.json.gz"), "wt") as fh:
			fh.write(out)

	def run_stride(self, force=False, minsselength=7):
		if os.path.isfile("assignments/opm_tms_stride.json.gz") and not force:
			return
		with gzip.open(self.dbpath.joinpath("assignments/opm_tms.json.gz"), "rt") as fh:
			obj = json.load(fh)

		if force or not self.dbpath.joinpath("assignments/stride.json.gz").exists():

			coords = {}
			stride = {}
			seenpdbs = set()
			for pdbc in tqdm.tqdm(obj):
				pdbid = pdbc[:4]
				##TODO: DEBUG STUFF
				#if pdbid not in ("5wau", "6met", "6iol", "3rfz"): continue

				if pdbid in seenpdbs: continue
				seenpdbs.add(pdbid)

				chain = pdbc[-1]
				if pdbid in stride: continue
				try: stride[pdbid] = self._get_stride(self.dbpath.joinpath("structures/pdb/{}.pdb".format(pdbid)))
				except FileNotFoundError:
					with open(self.dbpath.joinpath("assignments/missing_pdbs"), "a") as fh:
						fh.write(pdbid + "\n")

			with gzip.open(self.dbpath.joinpath("assignments/stride.json.gz"), "wt") as fh:
				fh.write("{")
				first = True
				for pdbid in sorted(stride):
					if first: first = False
					else: fh.write(",")
					fh.write('\n"{}": {}'.format(pdbid[:4], self._reformat_stride(stride[pdbid])).replace("'", '"').replace("None", "null"))
					
				fh.write("\n}\n")
		else:
			if self.verbosity > 0: info("{} already exists, skipping stride".format(self.dbpath.joinpath("assignments/stride.json.gz")))
			return

		#############################
		#if self.dbpath.joinpath("assignments/stride_vectors.json.gz").exists() and not force:
		#		#self.dbpath.joinpath("assignments/opm_tms_vectors.json.gz").exists() and 
		#	return

		with gzip.open(self.dbpath.joinpath("assignments/stride.json.gz"), "rt") as fh:
			obj = json.load(fh)

		ssecoords = {}

		errfh = open(self.dbpath.joinpath("assignments/stride_vectors_errors.log"), "w")
		for pdbid in tqdm.tqdm(obj):
			try:
				coords = self.get_coords(self.dbpath.joinpath("structures/pdb/{}.pdb".format(pdbid)))
			except FileNotFoundError:
				warn("Could not find a valid structure for pdbid {}".format(repr(pdbid)))
				continue

			for chain in obj[pdbid]:
				if len(chain) == 2: continue #intersubunit disulfide
				elif chain not in coords: warn("Could not find a valid chain for {} {}".format(pdbid, chain))

				for sse in obj[pdbid][chain]:
					if sse["resi"][0] > sse["resi"][1]:
						sse["vector"] = None
						errfh.write("Invalid SSE indices for {} chain {}: {}\n".format(pdbid, chain, str(sse)))
						continue
					ssepoints = [coords[chain][resi] for resi in range(sse["resi"][0], sse["resi"][1] + 1) if resi in coords[chain]]
					if not ssepoints: print(pdbid, chain, sse)

					pointarray = np.vstack(ssepoints)
					endpoint1, endpoint2 = self.get_endpoints(pointarray)

					sse["vector"] = [list(endpoint1), list(endpoint2)]

					centroid = np.mean(pointarray, axis=0)
					va, ve = np.linalg.eig(np.cov(pointarray.T))
					order = sorted([0, 1, 2], key=lambda i: np.abs(va[i]))
					va = va[order]
					ve = ve[:,order]
					if (endpoint2 - endpoint1) @ ve[:,0]: ve *= -1

					vestr = str([list(x) for x in ve])
					vastr = str(list(va))
					centroidstr = str(centroid)

		with gzip.open(self.dbpath.joinpath("assignments/stride_vectors.json.gz"), "wt") as fh:
			firststruc = True
			fh.write("{")
			for pdbid in sorted(obj):
				if firststruc: firststruc = False
				else: fh.write(",")
				fh.write('"{}": {{'.format(pdbid))
				firstchain = True

				for chain in sorted(obj[pdbid]):
					if firstchain: firstchain = False
					else: fh.write(",")
					fh.write('\n "{}": ['.format(chain))
					firstsse = True
					for sse in obj[pdbid][chain]:
						if firstsse: firstsse = False
						else: fh.write(",")
						fh.write("\n  " + str(sse).replace("'", '"').replace("None", "null"))
					fh.write("\n ]")
				fh.write("\n}")
			fh.write("\n}")

	def get_tms_vectors(self, force=False):
		with gzip.open(self.dbpath.joinpath("assignments/opm_tms_extended.json.gz"), "rt") as fh:
			obj = json.load(fh)

		if self.dbpath.joinpath("assignments/opm_tms_extended_vectors.json.gz").exists() and not force:
			if self.verbosity > 0: info("Skipping TMS vectorization because assignments/opm_tms_extended_vectors.json.gz already exists")
			return

		pdbcoords = {}
		outobj = {}
		errfh = open(self.dbpath.joinpath("assignments/opm_tms_extended_vectors_errors.log"), "w")
		for pdbc in tqdm.tqdm(obj):
			pdbid = pdbc[:4]
			##TODO: DEBUG STUFF
			#if pdbid not in ("5wau", "6met", "6iol", "3rfz"): continue
			chain = pdbc[-1]

			try:
				if pdbid in pdbcoords: coords = pdbcoords[pdbid]
				else: coords = pdbcoords[pdbid] = self.get_coords(self.dbpath.joinpath("structures/pdb/{}.pdb".format(pdbid)))
			except FileNotFoundError:
				warn("Could not find a valid structure for pdbid {}".format(repr(pdbid)))
				continue

			if pdbid not in outobj: outobj[pdbid] = {}
			if chain not in outobj[pdbid]: outobj[pdbid][chain] = []

			for tmsi, (start, end) in enumerate(obj[pdbc]):
				if chain not in coords:
					errfh.write("No resolved residues found for {} chain {}\n".format(pdbid, chain))
					continue
				coordlist = [coords[chain].get(resi) for resi in range(start, end+1) if coords[chain].get(resi) is not None]
				if not coordlist: 
					errfh.write("No resolved residues found for {} chain {} TMS {}\n".format(pdbid, chain, tmsi))
					outobj[pdbid][chain].append({
						"indices": [start, end],
						"endpoints": None,
						"eigenvalues": None,
					})
				else:
					xyz = np.vstack(coordlist)
					endpoints = self.get_endpoints(xyz)
					outobj[pdbid][chain].append({
						"indices": [start, end],
						"endpoints": [list(p) for p in endpoints],
						"eigenvalues": list(np.round(np.linalg.eig(np.cov(xyz.T))[0], 5))
					})

		with gzip.open(self.dbpath.joinpath("assignments/opm_tms_extended_vectors.json.gz"), "wt") as fh:
			fh.write("{")
			firststruc = True
			for pdbid in outobj:
				if firststruc: firststruc = False
				else: fh.write(",")
				fh.write('\n"{}": {{'.format(pdbid))
				firstchain = True
				for chain in outobj[pdbid]:
					if firstchain: firstchain = False
					else: fh.write(",")
					fh.write('\n "{}": ['.format(chain))
					firsttms = True
					for tms in outobj[pdbid][chain]:
						if firsttms: firsttms = False
						else: fh.write(",")
						fh.write("\n  {}".format(str(tms).replace("'", '"').replace("None", "null")))
					fh.write("]")
				fh.write("}")
			fh.write("}")


	def recommend_tms_extensions(self):
		pass


	def _reformat_stride(self, obj):
		out = {}
		for sse in obj:
			chain = sse.pop("chain")

			startresi = sse.pop("startresi")
			endresi = sse.pop("endresi")
			sse["resi"] = [startresi, endresi]
			if chain not in out: out[chain] = []
			out[chain].append(sse)
		return out

	def get_coords(self, fp, relevantnames=("N", "CA", "C")):
		coords = {}
		with open(fp) as fh:
			for l in fh:
				if l.startswith("ATOM") or l.startswith("HETATM"):
					resn = l[17:20]
					if resn not in PDB.protein_letters_3to1: continue
					name = l[13:15].strip()
					if name not in relevantnames: continue

					chain = l[21]
					resi = int(l[22:26])

					x = float(l[30:38])
					y = float(l[38:46])
					z = float(l[46:54])
					if chain not in coords: coords[chain] = {}
					if resi not in coords[chain]: coords[chain][resi] = np.zeros((0,3))
					coords[chain][resi] = np.vstack([coords[chain][resi], np.array([x, y, z])])
		return coords

	def get_endpoints(self, pointarray):
		#print(sse)
		centroid = np.mean(pointarray, axis=0)
		va, ve = np.linalg.eig(np.cov(pointarray.T))
		bestindex = np.nonzero(va)[0][0]

		#This rescaling makes SSE vectors saner to work with.
		overalldir = pointarray[-1,:] - pointarray[0,:]
		ve *= np.sign(overalldir @ ve[:,bestindex])

		#make sure at least 95% of all points are in [centroid-sqrt(2.5*PCA1), centroid+sqrt(2.5*PCA1)]
		pca1 = ve[:,bestindex]

		proj1 = (pointarray - centroid) @ ve[:,bestindex]
		endpoint1 = pointarray[np.nonzero(proj1 == np.min(proj1))[0][0]]
		endpoint2 = pointarray[np.nonzero(proj1 == np.max(proj1))[0][0]]

		return endpoint1, endpoint2

	
def main(args):
	db = CSVOPMTMData(args.outdir)

	db.parse_subunits(subunits=args.subunits, pdb=args.pdb)
	db.link_related(subunits=args.subunits, related=args.related, proteins=args.proteins)

	db.fetch_related(pdb=args.pdb)

	db.extract_sequences(pdb=args.pdb)
	db.extend_assignments()
	db.run_stride()
	db.get_tms_vectors()
	exit(0)
	db.recommend_tms_extensions()

	#TODO: delta BLASTing
	db.tcmap()


if __name__ == "__main__":
	parser = argparse.ArgumentParser()

	parser.add_argument("--subunits", required=True, type=pathlib.Path, help="subunits csv")
	parser.add_argument("--related", required=True, type=pathlib.Path, help="subunits csv")
	parser.add_argument("--proteins", required=True, type=pathlib.Path, help="proteins csv")
	parser.add_argument("--pdb", required=True, type=pathlib.Path, help="pdbs directory from all_pdbs.tar.gz")
	parser.add_argument("--outdir", required=True, type=pathlib.Path, help="output directory")

	args = parser.parse_args()

	main(args)
