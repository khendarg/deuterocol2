import numpy as np
import _curses
import curses

from . import log

class Camera(object):
	def __init__(self, position=None, forward=None, up=None):
		self.position = position if position is not None else np.array([0., 0., 0.])
		#self.forward = forward if forward is not None else np.array([1., 2, 2])
		#self.up = up if up is not None else np.array([-2., -1, 2])
		self.forward = forward if forward is not None else np.array([0., 0, 1])
		self.up = up if up is not None else np.array([0., 1, 0])
		self.forward /= np.linalg.norm(self.forward)
		self.up /= np.linalg.norm(self.up)

	def project(self, positions):
		right = np.cross(self.up, self.forward)
		matrix = np.vstack([right, self.up, self.forward]).T
		displacement = positions - self.position
		out = np.dot(displacement, matrix)
		return out

	def whatproject(self, positions):
		right = np.cross(self.up, self.forward)
		displacement = positions - self.position
		out = 1/np.linalg.norm(displacement, axis=1) 
		log.log(out.shape)
		out2 = np.hstack([
			right * np.dot(displacement, np.reshape(right, (3, 1))),
			self.up * np.dot(displacement, np.reshape(self.up, (3, 1))),
			self.forward * np.dot(displacement, np.reshape(self.forward, (3, 1)))
		]).T
		log.log(displacement.shape)
		log(out2.shape)
		log((right * np.dot(displacement, np.reshape(right, (3, 1)))).shape)
		return out

	#FIXME: accumulate angle operations instead of floating point errors
	def roll(self, angle):
		right = np.cross(self.up, self.forward)
		self.up = self.up * np.cos(angle) - right * np.sin(angle)
		self.up /= np.linalg.norm(self.up)

	def pitch(self, angle):
		newup = self.up * np.cos(angle) + self.forward * np.sin(angle)
		newforward = self.forward * np.cos(angle) - self.up * np.sin(angle)
		self.up = newup / np.linalg.norm(newup)
		self.forward = newforward / np.linalg.norm(newforward)

	def yaw(self, angle):
		right = np.cross(self.up, self.forward)
		newforward = self.forward * np.cos(angle) - right * np.sin(angle)
		self.forward = newforward / np.linalg.norm(newforward)

class BaseEntity(object):
	hidden = False
	def __init__(self, *args, **kwargs):
		pass
	def render(self): raise NotImplementedError

class Point(BaseEntity):
	def __init__(self, position, color=None, symbol="."):
		self.position = position
		self.color = color
		self.symbol = symbol

	def render(self):
		pass

	def get_bbox(self):
		pass

class PointCloud(BaseEntity):
	def __init__(self, positionlist, color=None, symbol="."):
		self.position = position
		self.color = color
		self.symbol = symbol

class NaiveRenderer(object):
	windows = None
	window = None
	cameras = None
	camera = None
	entities = None
	projection = "orthographic"

	aspect = 0.45 #width / height of single-width characters in terminal font 
	pixelscale = 0.2 #number of pixels per Ångstrom

	def __init__(self, **kwargs):
		self.windows = [] if kwargs.get("windows") is None else kwargs.get("windows")
		self.cameras = [] if kwargs.get("cameras") is None else kwargs.get("cameras")

		self.entities = []

	def add_window(self, window):
		self.windows.append(window)
		self.window = window
		self.cameras.append(Camera())
		self.camera = self.cameras[-1]

	#def add_structure(self, structure):
	#	for model in structure:

	def add_entity(self, entity):
		self.entities.append(entity)

	def get_positions(self):
		return np.vstack([[ent.position[i] for ent in self.entities] for i in range(3)]).T

	def autozoom(self):
		positions = self.get_positions()
		positions -= np.mean(positions, axis=0)
		projected = self.camera.project(positions)

		height, width = self.window.screen.getmaxyx()
		squishx = (np.max(projected[:,0]) - np.min(projected[:,0])) / width
		squishy = (np.max(projected[:,1]) - np.min(projected[:,1])) / height
		self.pixelwidth = 1 / max(squishx, squishy)


	def zoom(self):
		positions = self.get_positions()
		positions -= np.mean(positions, axis=0)
		projected = self.camera.project(positions)

		projected[:,0] /= self.aspect

		self.draw(projected)

	def get_color(self, color):
		#tmux doesn't support 256-color mode :(
		if color == "r": return curses.color_pair(1 + 1)
		elif color == "g": return curses.color_pair(1 + 2) 
		elif color == "y": return curses.color_pair(1 + 3)
		elif color == "b": return curses.color_pair(1 + 4)
		elif color == "m": return curses.color_pair(1 + 5)
		elif color == "c": return curses.color_pair(1 + 6)
		elif color == "w": return curses.color_pair(1 + 7)
		else: return 0

	def draw(self, projected):
		height, width = self.window.screen.getmaxyx()
		db = DrawBuffer(shape=(height, width), scale=self.pixelwidth)
		for ent, pos in zip(self.entities, projected): db.add(ent, pos)
		zlim = db.get_zlim()
		zspan = zlim[1] - zlim[0]

		def symbol_by_z(z):
			v = (z - zlim[0]) / zspan
			if v <= 0.2: return "."
			if v <= 0.4: return ","
			elif v <= 0.6: return "o"
			elif v <= 0.8: return "O"
			else: return "@"

		self.window.screen.clear()
		for row in np.arange(height, dtype=np.int64):
			#line = "".join([db.linebuffered[row][col].symbol if col in db.linebuffered[row] else " " for col in range(width)]) if row in db.linebuffered else ""
			#line = "".join([("o" if db.zbuffer[row,col] > 0 else ".") if col in db.linebuffered[row] else " " for col in range(width)]) if row in db.linebuffered else ""

			#line = "".join([(symbol_by_z(db.zbuffer[row,col])) if col in db.linebuffered[row] else " " for col in range(width)]) if row in db.linebuffered else ""
			#try: self.window.screen.addstr(row,0,line)
			#except _curses.error: pass

			if row not in db.linebuffered: continue
			for col in np.arange(width, dtype=np.int64):
				if col not in db.linebuffered[row]: continue
				self.window.screen.addstr(row, col, symbol_by_z(db.zbuffer[row,col]), self.get_color(db.entbuffer[row,col].color))

class DrawBuffer(object):
	def __init__(self, shape, scale):
		#assumes (y, x) order
		self.shape = np.array(shape)
		#in (y, x) order
		self.center = np.array([shape[1]//2, shape[0]//2])
		self.center3d = np.hstack([self.center, [0]])

		self.scale = scale

		self.zbuffer = {}
		self.entbuffer = {}
		self.linebuffered = {}

	def add(self, entity, position):
		if entity.hidden: return
		projected = np.int64(position * self.scale + self.center3d)
		z = position[2]

		if (projected[1], projected[0]) not in self:
			self.zbuffer[projected[1], projected[0]] = z
			self.entbuffer[projected[1], projected[0]] = entity

			if projected[1] not in self.linebuffered: self.linebuffered[projected[1]] = {}
			self.linebuffered[projected[1]][projected[0]] = entity
		else:
			#XXX sign flips may happen here!
			if projected[2] > self[projected[1],projected[0]]:
				self.zbuffer[projected[1]][projected[0]] = z
				self.entbuffer[projected[1],projected[0]] = entity

				if projected[1] not in self.linebuffered: self.linebuffered[projected[1]] = {}
				self.linebuffered[projected[1]][projected[0]] = entity

	def __contains__(self, indices):
		if indices[0] in self.zbuffer and indices[1] in self.zbuffer[indices[0]]: return True
		else: return False

	def __getitem__(self, indices):
		if indices not in self: return IndexError
		else: return self.zbuffer[indices[0]][indices[1]]

	def get_zlim(self):
		z = [self.zbuffer[_] for _ in self.zbuffer]
		return np.array([np.min(z), np.max(z)])
