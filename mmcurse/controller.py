import os
import numpy as np
from . import model
from . import renderer
from . import log

ANSIELEM = {"N":"b",
	"C":"g",
	"O":"r",
	"S":"y",
}
class Controller(object):
	def __init__(self, renderer, structures=None):
		self.structures = {} if structures is None else structures
		self.renderer = renderer

	def load(self, fp, format=None):
		structure = model.Structure(fp, format=format)
		self.structures[fp] = structure

	def render(self):
		#TODO: implement show/hide
		for strucname in self.structures:
			for model in self.structures[strucname]:
				for chain in model:
					for residue in chain:
						for atom in residue:
							self.renderer.add_entity(renderer.Point(atom.get_coord(), color=ANSIELEM.get(atom.element)))

		self.renderer.autozoom()

		while 1:
			#log.log(self.renderer.camera.position, self.renderer.camera.forward, self.renderer.camera.up)
			self.renderer.zoom()
			key = self.renderer.window.screen.getch()

			if key == ord("i"): self.renderer.camera.position += self.renderer.camera.up
			elif key == ord("j"): self.renderer.camera.position += np.cross(self.renderer.camera.forward, self.renderer.camera.up)
			elif key == ord("k"): self.renderer.camera.position += -self.renderer.camera.up
			elif key == ord("l"): self.renderer.camera.position += np.cross(self.renderer.camera.up, self.renderer.camera.forward)
			#elif key == ord("h"): self.renderer.camera.position += self.renderer.camera.forward
			#elif key == ord("n"): self.renderer.camera.position += -self.renderer.camera.forward
			elif key == ord("h"): self.renderer.pixelwidth *= 1.05
			elif key == ord("n"): self.renderer.pixelwidth /= 1.05

			elif key == ord("q"): self.renderer.camera.roll(0.05)
			elif key == ord("e"): self.renderer.camera.roll(-0.05)
			elif key == ord("w"): self.renderer.camera.pitch(0.05)
			elif key == ord("s"): self.renderer.camera.pitch(-0.05)
			elif key == ord("d"): self.renderer.camera.yaw(0.05)
			elif key == ord("a"): self.renderer.camera.yaw(-0.05)

			elif key == ord(" "): break

			elif key == ord("@"): 
				index = 0
				for strucname in self.structures:
					for model in self.structures[strucname]:
						for chain in model:
							for residue in chain:
								for atom in residue:
									if atom.name != "CA": self.renderer.entities[index].hidden = not self.renderer.entities[index].hidden
									index += 1
			elif key == ord("#"):
				index = 0
				for strucname in self.structures:
					for model in self.structures[strucname]:
						for chain in model:
							for residue in chain:
								for atom in residue:
									if atom.name not in ["CA", "N", "C", "O"]: self.renderer.entities[index].hidden = not self.renderer.entities[index].hidden
									index += 1

	
		#TODO: implement show/hide
