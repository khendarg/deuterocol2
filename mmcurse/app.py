from . import controller
from . import renderer
import curses
import kersatile

class App(object):
	def __init__(self, *args, **kwargs):
		self.renderer = kwargs.get("renderer", renderer.NaiveRenderer())
		self.controller = kwargs.get("controller", controller.Controller(renderer=self.renderer))

	def load(self, fn):
		self.controller.load(fn)

	def main(self, screen, **kwargs):
		self.screen = screen
		curses.start_color()
		curses.use_default_colors()
		for i in range(8):
			curses.init_pair(i + 1, i, -1)

		self.renderer.add_window(RenderWindow(self.screen))
		self.controller.render()

	def run(self):
		curses.wrapper(self.main)

class BaseWindow(object):
	def __init__(self, screen, *args, **kwargs):
		self.screen = screen
		self.args = args
		self.kwargs = kwargs
		self.run()

	def run(self):
		pass

class TestWindow(BaseWindow):
	def run(self):
		self.screen.addstr("Hello world from {}".format(self.screen))
		self.screen.getch()

class DebugPrintWindow(BaseWindow):
	def run(self):
		self.screen.addstr(" ".join([str(x) for x in self.args]))
		self.screen.getch()

class DebugKeyWindow(BaseWindow):
	def run(self):
		key = None
		lastkey = None
		self.screen.addstr("press qq to quit")
		while not (key == ord("q") and lastkey == ord("q")):
			lastkey = key
			key = self.screen.getch()
			self.screen.addstr("key: #{key}, '{chr}'".format(key=key, chr=chr(key) if 0x20 <= key < 0x100 else "???"))


class RenderWindow(BaseWindow):
	def run(self):
		self.screen.getch()
