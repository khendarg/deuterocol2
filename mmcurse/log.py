import time
def log(*stuff):
	with open("stdout.log", "a") as fh:
		fh.write(time.strftime("[%Y-%m-%d %H:%M:%S] "))
		fh.write(" ".join([str(x) for x in stuff]))
		fh.write("\n")
