from Bio import PDB

from . import StructureBuilder


import warnings
#warnings.filterwarnings("ignore", "[pwu][ads][ber]")
warnings.filterwarnings("ignore", ".*used.*")
warnings.filterwarnings("ignore", ".*invalid.*")
warnings.filterwarnings("ignore", ".*discontinuous.*")
warnings.filterwarnings("ignore", ".*defined twice.*")

class Structure(object):
	def __init__(self, fp=None, format=None):
		self.name = ""
		if fp is not None: self.load(fp, format=format)

	def load(self, fp, format="pdb"):
		if format is None:
			if str(fp).lower().endswith("cif"):
				p = PDB.MMCIFParser(structure_builder=StructureBuilder.MMCurseStructureBuilder())
			else:
				p = PDB.PDBParser(structure_builder=StructureBuilder.MMCurseStructureBuilder())
		elif format.lower() == "pdb":
			p = PDB.PDBParser(structure_builder=StructureBuilder.MMCurseStructureBuilder())
		elif format.lower() == "cif":
			p = PDB.MMCIFParser(structure_builder=StructureBuilder.MMCurseStructureBuilder())
		else: raise ValueError("Unknown format: '{}'".format(format))
		self.structure = p.get_structure(str(fp), fp)
		self.header = p.get_header()
		self.trailer = p.get_trailer()

	def __iter__(self): return self.structure.__iter__()
