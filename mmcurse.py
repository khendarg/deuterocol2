#!/usr/bin/env python
import argparse
import pathlib
import mmcurse

def main(args):
	app = mmcurse.app.App()
	for fn in args.infile:
		app.load(fn)
	app.run()

if __name__ == "__main__":
	parser = argparse.ArgumentParser()

	parser.add_argument("infile", nargs="*", type=pathlib.Path)
	args = parser.parse_args()

	main(args)
